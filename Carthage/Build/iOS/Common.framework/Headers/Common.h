//
//  Common.h
//  Common
//
//  Created by Bart Smaga on 03/04/2017.
//

#import <UIKit/UIKit.h>

//! Project version number for Common.
FOUNDATION_EXPORT double CommonVersionNumber;

//! Project version string for Common.
FOUNDATION_EXPORT const unsigned char CommonVersionString[];

// In this header, you should import all the public headers of your framework using statements like #import <Common/PublicHeader.h>

